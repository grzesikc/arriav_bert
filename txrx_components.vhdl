library ieee;
use ieee.std_logic_1164.all;

package txrx_components is

component altclkctrl is
	port (
		inclk  							: in  std_logic := '0'; --  altclkctrl_input.inclk
		outclk 							: out std_logic         -- altclkctrl_output.outclk
	);
end component altclkctrl;

component txrx_custom is
	port (
		phy_mgmt_clk                : in  std_logic                      := '0';             --                phy_mgmt_clk.clk
		phy_mgmt_clk_reset          : in  std_logic                      := '0';             --          phy_mgmt_clk_reset.reset
		phy_mgmt_address            : in  std_logic_vector(8 downto 0)   := (others => '0'); --                    phy_mgmt.address
		phy_mgmt_read               : in  std_logic                      := '0';             --                            .read
		phy_mgmt_readdata           : out std_logic_vector(31 downto 0);                     --                            .readdata
		phy_mgmt_waitrequest        : out std_logic;                                         --                            .waitrequest
		phy_mgmt_write              : in  std_logic                      := '0';             --                            .write
		phy_mgmt_writedata          : in  std_logic_vector(31 downto 0)  := (others => '0'); --                            .writedata
		tx_ready                    : out std_logic;                                         --                    tx_ready.export
		rx_ready                    : out std_logic;                                         --                    rx_ready.export
		pll_ref_clk                 : in  std_logic_vector(0 downto 0)   := (others => '0'); --                 pll_ref_clk.clk
		tx_serial_data              : out std_logic_vector(6 downto 0);                      --              tx_serial_data.export
		tx_forceelecidle            : in  std_logic_vector(6 downto 0)   := (others => '0'); --            tx_forceelecidle.export
		pll_locked                  : out std_logic_vector(0 downto 0);                      --                  pll_locked.export
		rx_serial_data              : in  std_logic_vector(6 downto 0)   := (others => '0'); --              rx_serial_data.export
		rx_runningdisp              : out std_logic_vector(6 downto 0);                      --              rx_runningdisp.export
		rx_is_lockedtoref           : out std_logic_vector(6 downto 0);                      --           rx_is_lockedtoref.export
		rx_is_lockedtodata          : out std_logic_vector(6 downto 0);                      --          rx_is_lockedtodata.export
		rx_signaldetect             : out std_logic_vector(6 downto 0);                      --             rx_signaldetect.export
		rx_patterndetect            : out std_logic_vector(6 downto 0);                      --            rx_patterndetect.export
		rx_syncstatus               : out std_logic_vector(6 downto 0);                      --               rx_syncstatus.export
		rx_bitslipboundaryselectout : out std_logic_vector(34 downto 0);                     -- rx_bitslipboundaryselectout.export
		tx_clkout                   : out std_logic_vector(6 downto 0);                      --                   tx_clkout.export
		rx_clkout                   : out std_logic_vector(6 downto 0);                      --                   rx_clkout.export
		tx_parallel_data            : in  std_logic_vector(55 downto 0)  := (others => '0'); --            tx_parallel_data.export
		tx_datak                    : in  std_logic_vector(6 downto 0)   := (others => '0'); --                    tx_datak.export
		rx_parallel_data            : out std_logic_vector(55 downto 0);                     --            rx_parallel_data.export
		rx_datak                    : out std_logic_vector(6 downto 0);                      --                    rx_datak.export
		reconfig_from_xcvr          : out std_logic_vector(643 downto 0);                    --          reconfig_from_xcvr.reconfig_from_xcvr
		reconfig_to_xcvr            : in  std_logic_vector(979 downto 0) := (others => '0')  --            reconfig_to_xcvr.reconfig_to_xcvr
	);
end component txrx_custom;

component txrx_reconfig is
	port (
		reconfig_busy             : out std_logic;                                         --      reconfig_busy.reconfig_busy
		mgmt_clk_clk              : in  std_logic                      := '0';             --       mgmt_clk_clk.clk
		mgmt_rst_reset            : in  std_logic                      := '0';             --     mgmt_rst_reset.reset
		reconfig_mgmt_address     : in  std_logic_vector(6 downto 0)   := (others => '0'); --      reconfig_mgmt.address
		reconfig_mgmt_read        : in  std_logic                      := '0';             --                   .read
		reconfig_mgmt_readdata    : out std_logic_vector(31 downto 0);                     --                   .readdata
		reconfig_mgmt_waitrequest : out std_logic;                                         --                   .waitrequest
		reconfig_mgmt_write       : in  std_logic                      := '0';             --                   .write
		reconfig_mgmt_writedata   : in  std_logic_vector(31 downto 0)  := (others => '0'); --                   .writedata
		reconfig_to_xcvr          : out std_logic_vector(979 downto 0);                    --   reconfig_to_xcvr.reconfig_to_xcvr
		reconfig_from_xcvr        : in  std_logic_vector(643 downto 0) := (others => '0')  -- reconfig_from_xcvr.reconfig_from_xcvr
	);
end component txrx_reconfig;

component tx_custom is
	port (
		phy_mgmt_clk         : in  std_logic                      := '0';             --       phy_mgmt_clk.clk
		phy_mgmt_clk_reset   : in  std_logic                      := '0';             -- phy_mgmt_clk_reset.reset
		phy_mgmt_address     : in  std_logic_vector(8 downto 0)   := (others => '0'); --           phy_mgmt.address
		phy_mgmt_read        : in  std_logic                      := '0';             --                   .read
		phy_mgmt_readdata    : out std_logic_vector(31 downto 0);                     --                   .readdata
		phy_mgmt_waitrequest : out std_logic;                                         --                   .waitrequest
		phy_mgmt_write       : in  std_logic                      := '0';             --                   .write
		phy_mgmt_writedata   : in  std_logic_vector(31 downto 0)  := (others => '0'); --                   .writedata
		tx_ready             : out std_logic;                                         --           tx_ready.export
		pll_ref_clk          : in  std_logic_vector(0 downto 0)   := (others => '0'); --        pll_ref_clk.clk
		tx_serial_data       : out std_logic_vector(2 downto 0);                      --     tx_serial_data.export
		pll_locked           : out std_logic_vector(0 downto 0);                      --         pll_locked.export
		tx_clkout            : out std_logic_vector(2 downto 0);                      --          tx_clkout.export
		tx_parallel_data     : in  std_logic_vector(23 downto 0)  := (others => '0'); --   tx_parallel_data.export
		tx_datak             : in  std_logic_vector(2 downto 0)   := (others => '0'); --           tx_datak.export
		reconfig_from_xcvr   : out std_logic_vector(275 downto 0);                    -- reconfig_from_xcvr.reconfig_from_xcvr
		reconfig_to_xcvr     : in  std_logic_vector(419 downto 0) := (others => '0')  --   reconfig_to_xcvr.reconfig_to_xcvr
	);
end component tx_custom;

component tx_reconfig is
	port (
		reconfig_busy             : out std_logic;                                         --      reconfig_busy.reconfig_busy
		mgmt_clk_clk              : in  std_logic                      := '0';             --       mgmt_clk_clk.clk
		mgmt_rst_reset            : in  std_logic                      := '0';             --     mgmt_rst_reset.reset
		reconfig_mgmt_address     : in  std_logic_vector(6 downto 0)   := (others => '0'); --      reconfig_mgmt.address
		reconfig_mgmt_read        : in  std_logic                      := '0';             --                   .read
		reconfig_mgmt_readdata    : out std_logic_vector(31 downto 0);                     --                   .readdata
		reconfig_mgmt_waitrequest : out std_logic;                                         --                   .waitrequest
		reconfig_mgmt_write       : in  std_logic                      := '0';             --                   .write
		reconfig_mgmt_writedata   : in  std_logic_vector(31 downto 0)  := (others => '0'); --                   .writedata
		reconfig_to_xcvr          : out std_logic_vector(419 downto 0);                    --   reconfig_to_xcvr.reconfig_to_xcvr
		reconfig_from_xcvr        : in  std_logic_vector(275 downto 0) := (others => '0')  -- reconfig_from_xcvr.reconfig_from_xcvr
	);
end component tx_reconfig;


end package txrx_components;