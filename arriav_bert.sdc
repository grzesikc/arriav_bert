
#**************************************************************
# Create Clock
#**************************************************************
create_clock -period  "50.000000 MHz" [get_ports clkin_50_top]
create_clock -period  "125.000000 MHz" [get_ports refclk2_qr1_p]

#**************************************************************
# Create Generated Clock
#**************************************************************
derive_pll_clocks -create_base_clocks
derive_clock_uncertainty


#**************************************************************
# Set False Path
#**************************************************************
#set_false_path -from [get_clocks {CLK_50_B2J}] -to [get_clocks {pcie_b|pcie_if|pcie_a10_hip_0|coreclkout}]
set_false_path -to [get_ports {user_led[*]}]