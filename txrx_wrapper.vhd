library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;

use work.txrx_components.all;

entity txrx_wrapper is
	port (
		refclk125			: in std_logic;
		user_rst_n			: in std_logic;
		rx_serial_data		: in std_logic_vector(6 downto 0);
		tx_serial_data		: out std_logic_vector(6 downto 0);
		tx_clkout 			: out std_logic_vector (6 downto 0);
		rx_clkout 			: out std_logic_vector (6 downto 0);
		tx_data				: in std_logic_vector (55 downto 0);
		tx_data_k			: in std_logic_vector (6 downto 0);
		rx_data				: out std_logic_vector (55 downto 0);
		rx_data_k			: out std_logic_vector (6 downto 0);
		tx_ready				: out std_logic;
		rx_ready				: out std_logic;
		rx_is_lockedtoref : out std_logic_vector(6 downto 0);                      --           rx_is_lockedtoref.export
		rx_is_lockedtodata : out std_logic_vector(6 downto 0);                      --          rx_is_lockedtodata.export
		rx_signaldetect   : out std_logic_vector(6 downto 0);      
		rx_patterndetect	: out std_logic_vector (6 downto 0);
		rx_syncstatus		: out std_logic_vector (6 downto 0);
		pll_locked			: out std_logic;
		reconfig_busy		: out std_logic
	);
end entity txrx_wrapper;

architecture rtl of txrx_wrapper is

signal reconfig_from_xcvr_i			: std_logic_vector(643 downto 0);
signal reconfig_to_xcvr_i				: std_logic_vector(979 downto 0);
--signal refclk125_i						: std_logic;


begin

--	clk_controller : altclkctrl
--	port map(
--		inclk  							=> refclk125,
--		outclk 							=> refclk125_i
--	);

	transceiver: txrx_custom
	port map (
		phy_mgmt_clk                => refclk125,
		phy_mgmt_clk_reset          => user_rst_n,
		phy_mgmt_address            => (others => '0'),
		phy_mgmt_read               => '0',
--		phy_mgmt_readdata           : out std_logic_vector(31 downto 0);                     --                            .readdata
--		phy_mgmt_waitrequest        : out std_logic;                                         --                            .waitrequest
		phy_mgmt_write              => '0',
		phy_mgmt_writedata          => (others => '0'),
		tx_ready                    => tx_ready,
		rx_ready                    => rx_ready,
		pll_ref_clk(0)              => refclk125,
		tx_serial_data              => tx_serial_data,
		pll_locked(0)               => pll_locked,
		rx_serial_data              => rx_serial_data,
--		rx_runningdisp              : out std_logic_vector(3 downto 0);                      --              rx_runningdisp.export
		rx_is_lockedtoref           => rx_is_lockedtoref,
		rx_is_lockedtodata          => rx_is_lockedtodata,
		rx_signaldetect             => rx_signaldetect,
		rx_patterndetect            => rx_patterndetect,
		rx_syncstatus               => rx_syncstatus,
--		rx_bitslipboundaryselectout : out std_logic_vector(19 downto 0);                     -- rx_bitslipboundaryselectout.export
		tx_clkout                   => tx_clkout,
		rx_clkout                   => rx_clkout,
		tx_parallel_data            => tx_data,
		tx_datak                    => tx_data_k,
		rx_parallel_data            => rx_data,
		rx_datak                    => rx_data_k,
		reconfig_from_xcvr          => reconfig_from_xcvr_i,              						 --          reconfig_from_xcvr.reconfig_from_xcvr
		reconfig_to_xcvr            => reconfig_to_xcvr_i											 --            reconfig_to_xcvr.reconfig_to_xcvr
	);

	transceiver_reconfig : txrx_reconfig
	port map (
		reconfig_busy             => reconfig_busy,
		mgmt_clk_clk              => refclk125,
		mgmt_rst_reset            => user_rst_n,
		reconfig_mgmt_address     => (others => '0'),
		reconfig_mgmt_read        => '0',
--		reconfig_mgmt_readdata    => (others => '0'),
--		reconfig_mgmt_waitrequest : out std_logic;                                         --                   .waitrequest
		reconfig_mgmt_write       => '0',
		reconfig_mgmt_writedata   => (others => '0'),
		reconfig_to_xcvr          => reconfig_to_xcvr_i,
		reconfig_from_xcvr        => reconfig_from_xcvr_i
	);
	
end;