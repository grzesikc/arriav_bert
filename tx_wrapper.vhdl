library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;

use work.txrx_components.all;

entity tx_wrapper is
	port (
		refclk125			: in std_logic;
		user_rst_n			: in std_logic;
		tx_serial_data		: out std_logic_vector(2 downto 0);
		tx_clkout 			: out std_logic_vector (2 downto 0);
		tx_data				: in std_logic_vector (23 downto 0);
		tx_ready				: out std_logic;
		pll_locked			: out std_logic;
		reconfig_busy		: out std_logic
	);
end entity tx_wrapper;

architecture rtl of tx_wrapper is

signal reconfig_from_xcvr_i			: std_logic_vector(275 downto 0);
signal reconfig_to_xcvr_i				: std_logic_vector(419 downto 0);
--signal refclk125_i						: std_logic;


begin
	
transmitter : tx_custom
	port map (
		phy_mgmt_clk         => refclk125,
		phy_mgmt_clk_reset   => user_rst_n,
		phy_mgmt_address     => (others => '0'),
		phy_mgmt_read        => '0',
--		phy_mgmt_readdata    : out std_logic_vector(31 downto 0);                     --                   .readdata
--		phy_mgmt_waitrequest : out std_logic;                                         --                   .waitrequest
		phy_mgmt_write       => '0',
		phy_mgmt_writedata   => (others => '0'),
		tx_ready             => tx_ready,
		pll_ref_clk(0)       => refclk125,
		tx_serial_data       => tx_serial_data,
		pll_locked(0)        => pll_locked,
		tx_clkout            => tx_clkout,
		tx_parallel_data     => tx_data,
		tx_datak             => (others => '0'),
		reconfig_from_xcvr   => reconfig_from_xcvr_i,
		reconfig_to_xcvr     => reconfig_to_xcvr_i
	);

transmitter_reconfig : tx_reconfig
	port map(
		reconfig_busy             => reconfig_busy,
		mgmt_clk_clk              => refclk125,
		mgmt_rst_reset            => user_rst_n,
		reconfig_mgmt_address     => (others => '0'),
		reconfig_mgmt_read        => '0',
--		reconfig_mgmt_readdata    : out std_logic_vector(31 downto 0);                     --                   .readdata
--		reconfig_mgmt_waitrequest : out std_logic;                                         --                   .waitrequest
		reconfig_mgmt_write       => '0',
		reconfig_mgmt_writedata   => (others => '0'),
		reconfig_to_xcvr          => reconfig_to_xcvr_i,
		reconfig_from_xcvr        => reconfig_from_xcvr_i
	);

end;