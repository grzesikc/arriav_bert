-- Components library for arriav_bert
library ieee;
use ieee.std_logic_1164.all;

package components is

-- push button debouncer
component debounce is
    generic (
        N   : integer := 1;
        C   : std_logic_vector := X"FFFF"--;
    );
    port (
        clk     :   in  std_logic;
        input   :   in  std_logic_vector(N-1 downto 0);
        output  :   out std_logic_vector(N-1 downto 0)--;
    );
end component debounce;

component txrx_wrapper is
	port (
		refclk125			: in std_logic;
		user_rst_n			: in std_logic;
		rx_serial_data		: in std_logic_vector(6 downto 0);
		tx_serial_data		: out std_logic_vector(6 downto 0);
		tx_clkout 			: out std_logic_vector (6 downto 0);
		rx_clkout 			: out std_logic_vector (6 downto 0);
		tx_data				: in std_logic_vector (55 downto 0);
		tx_data_k			: in std_logic_vector (6 downto 0);
		rx_data				: out std_logic_vector (55 downto 0);
		rx_data_k			: out std_logic_vector (6 downto 0);
		tx_ready				: out std_logic;
		rx_ready				: out std_logic;
		rx_is_lockedtoref : out std_logic_vector(6 downto 0);                      --           rx_is_lockedtoref.export
		rx_is_lockedtodata : out std_logic_vector(6 downto 0);                      --          rx_is_lockedtodata.export
		rx_signaldetect   : out std_logic_vector(6 downto 0);      
		rx_patterndetect	: out std_logic_vector (6 downto 0);
		rx_syncstatus		: out std_logic_vector (6 downto 0);
		pll_locked			: out std_logic;
		reconfig_busy		: out std_logic
	);
end component txrx_wrapper;

component tx_wrapper is
	port (
		refclk125			: in std_logic;
		user_rst_n			: in std_logic;
		tx_serial_data		: out std_logic_vector(2 downto 0);
		tx_clkout 			: out std_logic_vector (2 downto 0);
		tx_data				: in std_logic_vector (23 downto 0);
		tx_ready				: out std_logic;
		pll_locked			: out std_logic;
		reconfig_busy		: out std_logic
	);
end component tx_wrapper;


--component altclkctrl is
--	port (
--		inclk  							: in  std_logic := '0'; --  altclkctrl_input.inclk
--		outclk 							: out std_logic         -- altclkctrl_output.outclk
--	);
--end component altclkctrl;

	
component lcd_hex_converter
port(
	din:			in std_logic_vector(3 downto 0);
	dout:			out std_logic_vector(7 downto 0)
	);		
end component;



component lcd_controller
port(
	din:			in std_logic_vector(127 downto 0);
	clock: in std_logic;												-- Reference Clock
	reset_n: in std_logic;											-- Manual reset_n
	enable_in: in std_logic;										-- external LCD enable
	lcd_enable: out std_logic;										-- On/Off Switch, connect to lcd_csn, 1 is on, 0 is off
	lcd_rs: out std_logic;											-- Data/Setup Switch, connect to lcd_d_cn, 1 is data, 0 is setup
	lcd_rw: out std_logic;											-- Read/Write Switch, connect to lcd_wen, 1 is read, 0 is write
	busy: out std_logic;												-- Busy Feedback
	lcd_data: out std_logic_vector (7 downto 0);					-- LCD Data
	ready_out: out std_logic										-- ready signal
	);		
end component;
	
-- Character encoding for LCD device
		
		constant a: std_logic_vector (7 downto 0) := "01100001";
		constant b: std_logic_vector (7 downto 0) := "01100010";
		constant c: std_logic_vector (7 downto 0) := "01100011";
		constant d: std_logic_vector (7 downto 0) := "01100100";
		constant e: std_logic_vector (7 downto 0) := "01100101";
		constant f: std_logic_vector (7 downto 0) := "01100110";
		constant g: std_logic_vector (7 downto 0) := "01100111";
		constant h: std_logic_vector (7 downto 0) := "01101000";
		constant i: std_logic_vector (7 downto 0) := "01101001";
		constant j: std_logic_vector (7 downto 0) := "01101010";
		constant k: std_logic_vector (7 downto 0) := "01101011";
		constant l: std_logic_vector (7 downto 0) := "01101100";
		constant m: std_logic_vector (7 downto 0) := "01101101";
		constant n: std_logic_vector (7 downto 0) := "01101110";
		constant o: std_logic_vector (7 downto 0) := "01101111";
		constant p: std_logic_vector (7 downto 0) := "01110000";
		constant q: std_logic_vector (7 downto 0) := "01110001";
		constant r: std_logic_vector (7 downto 0) := "01110010";
		constant s: std_logic_vector (7 downto 0) := "01110011";
		constant t: std_logic_vector (7 downto 0) := "01110100";
		constant u: std_logic_vector (7 downto 0) := "01110101";
		constant v: std_logic_vector (7 downto 0) := "01110110";
		constant w: std_logic_vector (7 downto 0) := "01110111";
		constant x: std_logic_vector (7 downto 0) := "01111000";
		constant y: std_logic_vector (7 downto 0) := "01111001";
		constant z: std_logic_vector (7 downto 0) := "01111010";
		
		constant acap: std_logic_vector (7 downto 0) := "01000001";
		constant bcap: std_logic_vector (7 downto 0) := "01000010";
		constant ccap: std_logic_vector (7 downto 0) := "01000011";
		constant dcap: std_logic_vector (7 downto 0) := "01000100";
		constant ecap: std_logic_vector (7 downto 0) := "01000101";
		constant fcap: std_logic_vector (7 downto 0) := "01000110";
		constant gcap: std_logic_vector (7 downto 0) := "01000111";
		constant hcap: std_logic_vector (7 downto 0) := "01001000";
		constant icap: std_logic_vector (7 downto 0) := "01001001";
		constant jcap: std_logic_vector (7 downto 0) := "01001010";
		constant kcap: std_logic_vector (7 downto 0) := "01001011";
		constant lcap: std_logic_vector (7 downto 0) := "01001100";
		constant mcap: std_logic_vector (7 downto 0) := "01001101";
		constant ncap: std_logic_vector (7 downto 0) := "01001110";
		constant ocap: std_logic_vector (7 downto 0) := "01001111";
		constant pcap: std_logic_vector (7 downto 0) := "01010000";
		constant qcap: std_logic_vector (7 downto 0) := "01010001";
		constant rcap: std_logic_vector (7 downto 0) := "01010010";
		constant scap: std_logic_vector (7 downto 0) := "01010011";
		constant tcap: std_logic_vector (7 downto 0) := "01010100";
		constant ucap: std_logic_vector (7 downto 0) := "01010101";
		constant vcap: std_logic_vector (7 downto 0) := "01010110";
		constant wcap: std_logic_vector (7 downto 0) := "01010111";
		constant xcap: std_logic_vector (7 downto 0) := "01011000";
		constant ycap: std_logic_vector (7 downto 0) := "01011001";
		constant zcap: std_logic_vector (7 downto 0) := "01011010";
		
		constant num0: std_logic_vector (7 downto 0) := "00110000";
		constant num1: std_logic_vector (7 downto 0) := "00110001";
		constant num2: std_logic_vector (7 downto 0) := "00110010";
		constant num3: std_logic_vector (7 downto 0) := "00110011";
		constant num4: std_logic_vector (7 downto 0) := "00110100";
		constant num5: std_logic_vector (7 downto 0) := "00110101";
		constant num6: std_logic_vector (7 downto 0) := "00110110";
		constant num7: std_logic_vector (7 downto 0) := "00110111";
		constant num8: std_logic_vector (7 downto 0) := "00111000";
		constant num9: std_logic_vector (7 downto 0) := "00111001";
		
		constant at: std_logic_vector (7 downto 0) := "01000000";
		constant space: std_logic_vector (7 downto 0) := "00100000";
		constant exclam: std_logic_vector (7 downto 0) := "00100001";
		constant quote: std_logic_vector (7 downto 0) := "00100010";
		constant hash: std_logic_vector (7 downto 0) := "00100011";
		constant dollar: std_logic_vector (7 downto 0) := "00100100";
		constant percent: std_logic_vector (7 downto 0) := "00100101";
		constant andsign: std_logic_vector (7 downto 0) := "00100110";
		constant apostrophe: std_logic_vector (7 downto 0) := "00100111";
		constant leftbracket: std_logic_vector (7 downto 0) := "00101000";
		constant rightbracket: std_logic_vector (7 downto 0) := "00101001";
		constant star: std_logic_vector (7 downto 0) := "00101010";
		constant plus: std_logic_vector (7 downto 0) := "00101011";
		constant comma: std_logic_vector (7 downto 0) := "00101100";
		constant bar: std_logic_vector (7 downto 0) := "00101101";
		constant dot: std_logic_vector (7 downto 0) := "00101110";
		constant slash: std_logic_vector (7 downto 0) := "00101111";
		constant colon: std_logic_vector (7 downto 0) := "00111010";
		constant semicolon: std_logic_vector (7 downto 0) := "00111011";
		constant less: std_logic_vector (7 downto 0) := "00111100";
		constant equal: std_logic_vector (7 downto 0) := "00111011";
		constant more: std_logic_vector (7 downto 0) := "00111110";
		constant question: std_logic_vector (7 downto 0) := "00111111";
		constant leftsquarebracket: std_logic_vector (7 downto 0) := "01011011";
		constant rightsquarebracket: std_logic_vector (7 downto 0) := "01011101";
		constant circumflex: std_logic_vector (7 downto 0) := "01011110";
		constant bottombar: std_logic_vector (7 downto 0) := "01011111";
		constant rightarrow: std_logic_vector (7 downto 0) := "01111110";
		constant leftarrow: std_logic_vector (7 downto 0) := "01111111";	

end package components;