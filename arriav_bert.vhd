------------------------------------------------------------------------------
-- Title:       a5gx_starter_golden_top.vhd                                   
-- Rev:         Rev 1.0                                                     
------------------------------------------------------------------------------
-- Description: Golden Top file contains Arria V GX FPGA Starter Kit		
--              Rev C board pins and I/O standards.                         
------------------------------------------------------------------------------
-- Revision History:                                                        
-- Rev 1.0: Initial release															
------------------------------------------------------------------------------
-------- 1 ------- 2 ------- 3 ------- 4 ------- 5 ------- 6 ------- 7 ------7
-------- 0 ------- 0 ------- 0 ------- 0 ------- 0 ------- 0 ------- 0 ------8
------------------------------------------------------------------------------
--Copyright 2012 Altera Corporation. All rights reserved.  Altera products
--are protected under numerous U.S. and foreign patents, maskwork rights,
--copyrights and other intellectual property laws.
--                 
--This reference design file, and your use thereof, is subject to and
--governed by the terms and conditions of the applicable Altera Reference
--Design License Agreement.  By using this reference design file, you
--indicate your acceptance of such terms and conditions between you and
--Altera Corporation.  In the event that you do not agree with such terms and
--conditions, you may not use the reference design file. Please promptly                         
--destroy any copies you have made.
--
--This reference design file being provided on an "as-is" basis and as an
--accommodation and therefore all warranties, representations or guarantees
--of any kind (whether express, implied or statutory) including, without
--limitation, warranties of merchantability, non-infringement, or fitness for
--a particular purpose, are specifically disclaimed.  By making this
--reference design file available, Altera expressly does not recommend,
--suggest or require that this reference design file be used in combination 
--with any other product not provided by Altera
------------------------------------------------------------------------------

-- BERT on Arria V Starter Kit Board
-- to test Interposer for P2 readout
--
-- Carsten Grzesik
-- cgrzesik@uni-mainz.de
-- January 2018
--
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;

use work.components.all;

entity arriav_bert is
	port 
	(
	--Refclk & Clkout
	--clkin: 22
--	--clkout: 1
--	clk_148_p		: in std_logic;--					1.5-V PCML, default 148.5MHz
--	refclk1_ql0_p	: in std_logic;--					1.5-V PCML, default 409.6MHz
--	refclk2_ql1_p	: in std_logic;--					1.5-V PCML, default 125MHz
--	refclk1_qr0_p	: in std_logic;--					1.5-V PCML, default 156.25MHz
	refclk2_qr1_p	: in std_logic;--					1.5-V PCML, default 125MHz
--	refclk3_qr1_p	: in std_logic;--					1.5-V PCML, HSMC CLKIN2 (differential)
--	clk_125_p		: in std_logic;--					LVDS, default 125MHz
--	clkintop_125_p	: in std_logic;--					LVDS, default 125MHz
--	clkinbot_125_p	: in std_logic;--					LVDS, default 125MHz
--	clkintop_100_p	: in std_logic;--					LVDS, default 100MHz
--	clkinbot_100_p	: in std_logic;--					LVDS, default 100MHz
	clkin_50_top	: in std_logic;--					2.5V, default 50MHz
--	clkin_50_bot	: in std_logic;--					2.5V, default 50MHz
--	clkout_sma		: in std_logic;--					2.5V
--
--	--SMA connector interface
--	--xcvr: 1
--	sma_xcvr_tx_p	: out std_logic;--				1.5-V PCML, SMA Receive	
--	sma_xcvr_rx_p	: in std_logic;--					1.5-V PCML, SMA Transmit	
--	
--	--PCIe edge connector interface
--	--xcvr: 8
--	--IO: 8
--	pcie_tx_p		: out std_logic_vector(7 downto 0);--	1.5-V PCML, PCIe Transmit Data
--	pcie_rx_p		: in std_logic_vector(7 downto 0);--	1.5-V PCML, PCIe Receive Data-req's OCT
--	pcie_refclk_p	: in std_logic;--	 	 						HCSL, PCIe Clock- Terminate on MB	
--	pcie_perstn		: in std_logic;--	 							2.5V, PCIe Reset
--	pcie_led_g2		: out std_logic;--		 					2.5V, PCIe GEN2 LED indicator
--	pcie_waken		: out std_logic;--			 				2.5V, PCIe Wake-Up
--	pcie_led_x1		: out std_logic;--		 					2.5V, PCIe x1 LED indicator
--	pcie_led_x4		: out std_logic;--		 					2.5V, PCIe x4 LED indicator
--	pcie_led_x8		: out std_logic;--			 				2.5V, PCIe x8 LED indicator
--	pcie_smbdat		: inout std_logic;--							2.5V, SMBus Clock
--	pcie_smbclk		: out std_logic;--		 					2.5V, SMBus Data
--
--	--SDI cable driver/equalizer interface 
--	--xcvr: 1
--	--IO: 9
--	sdi_tx_p			: out std_logic;--						1.5-V PCML, SDI Video Output
--	sdi_rx_p			: in std_logic;--							1.5-V PCML, SDI Video Input-req's OCT
--	sdi_rx_bypass	: in std_logic;--							2.5V, Receive Bypass
--	sdi_tx_sd_hdn	: out std_logic;--						2.5V, HD Mode Enable
--	sdi_tx_en		: out std_logic;--						2.5V, Transmit Enable
--	sdi_rx_en		: out std_logic;--						2.5V, Receive Enable - Tri-state
--	sdi_scl			: out std_logic;--						2.5V, Cable driver I2C SCL
--	sdi_sda			: inout std_logic;--						2.5V, Cable driver I2C SDA
--	sdi_fault		: in std_logic;--							2.5V, Cable driver Fault
--	sdi_clk148_up	: out std_logic;--						2.5V, VCO Frequency Up
--	sdi_clk148_dn	: out std_logic;--						2.5V, VCO Frequency Down
--
--	--HDMI interface
--	--IO: 4
--	--XCVR: 4 (TX only)
--	hdmi_tx_i2c_scl	: out std_logic;--							2.5V, DDC SCL
--	hdmi_tx_i2c_sda	: inout std_logic;--							2.5V, DDC SDA
--	hdmi_tx_oen			: out std_logic;--							2.5V, HDMI Transmit enable
--	hdmi_tx_hpd			: in std_logic;--								2.5V, HDMI Hot Plug Detect
--	hdmi_tx_p			: out std_logic_vector(2 downto 0);--	1.5-V PCML, HDMI Transmit Data (2.7Gbps max)
--	hdmi_tx_clkout_p	: out std_logic;--							1.5-V PCML, HDMI Transmit Clock (270MHz max)
--
--   --HIGH-SPEED-MEZZANINE-CARD interface ------------//198 pins
--   --xcvr: 8
--   --IO: 85
--
--   --Bank 1 (transceivers)   	
	hsma_tx_p	: out std_logic_vector(7 downto 1);--			1.5-V PCML, HSMA Transmit Data
	hsma_rx_p	: in std_logic_vector(7 downto 1);--			1.5-V PCML, HSMA Receive Data-req's OCT
--	
--	--Bank 1, 2 & 3 (non-transceiver)
--	hsma_sda				: inout std_logic;--         					2.5V, SMBus Data   
--	hsma_scl				: out std_logic;--         					2.5V, SMBus Clock 
--	hsma_prsntn			: in std_logic;--									2.5V, HSMC Presence Detect Input   
--	hsma_clk_out0		: out std_logic;--	   						2.5V, Primary single-ended CLKOUT
--	hsma_clk_in0	 	: in std_logic;--	   							2.5V, Primary single-ended CLKIN
--	hsma_clk_out_p		: out std_logic_vector(2 downto 1);--		LVDS, Primary & Secondary Source-Sync CLKOUT
--	hsma_clk_in_p		: in std_logic_vector(2 downto 1);--		LVDS, Primary & Secondary Source-Sync CLKIN
--	hsma_d				: inout std_logic_vector(3 downto 0);--	2.5V, Dedicated CMOS IO
--	hsma_tx_d_p			: out std_logic_vector(16 downto 0);--		LVDS, LVDS Sounce-Sync Output
--	hsma_rx_d_p			: in std_logic_vector(16 downto 0);--		LVDS, LVDS Sounce-Sync Input

-- HSMC-Port-A / santaluz -- 107 pins
---- leds
--   hsma_sl_stat_led    : out   std_logic_vector(7 downto 0); -- User LEDs
--   hsma_sl_rx_led      : out   std_logic_vector(7 downto 0); -- User LEDs
--   hsma_sl_tx_led      : out   std_logic_vector(7 downto 0); -- User LEDs
---- slow control (TODO)
--   hsma_sl_tx_disable  : out   std_logic_vector(7 downto 0);
--   hsma_sl_rate_sel    : out   std_logic_vector(7 downto 0); -- sfp+ have two modes
--   hsma_sl_sfp_present : in    std_logic_vector(7 downto 0); -- detect hardware
--   hsma_sl_sfp_scl     : out   std_logic_vector(7 downto 0); -- slow control clock
--   hsma_sl_sfp_sda     : inout std_logic_vector(7 downto 0); -- slow control signal 
--   hsma_sl_rx_loss     : in    std_logic_vector(7 downto 0); -- hardware signal loss
--   hsma_sl_tx_fault : in std_logic_vector(7 downto 0); -- ... fault
--  sma_sl_ : out std_logic_vector(7 downto 0);



--	
--	--DDR3 x32 Devices Interface
--	--IO: 70
--	ddr3top_ck_p		: out std_logic;--								DIFFERENTIAL 1.5-V SSTL CLASS I, Diff Clock - Pos
--	ddr3top_ck_n		: out std_logic;--								DIFFERENTIAL 1.5-V SSTL CLASS I, Diff Clock - Neg
--	ddr3top_dq			: inout std_logic_vector(31 downto 0);--	SSTL-15 CLASS I, Data Bus
--	ddr3top_dqs_p		: inout std_logic_vector(3 downto 0);--	DIFFERENTIAL 1.5-V SSTL CLASS I, Diff Data Strobe - Pos
--	ddr3top_dqs_n		: inout std_logic_vector(3 downto 0);--	DIFFERENTIAL 1.5-V SSTL CLASS I, Diff Data Strobe - Neg
--	ddr3top_dm			: out std_logic_vector(3 downto 0);--		SSTL-15 CLASS I, Data Write Mask
--	ddr3top_a			: out std_logic_vector(12 downto 0);--		SSTL-15 CLASS I, Address
--	ddr3top_ba			: out std_logic_vecto							SSTL-15 CLASS I, Column Address Strobe
--	ddr3top_rasn		: out std_logic;--r(2 downto 0);--		SSTL-15 CLASS I, Bank Address
--	ddr3top_casn		: out std_logic;--									SSTL-15 CLASS I, Row Address Strobe
--	ddr3top_cke			: out std_logic;--								SSTL-15 CLASS I, Clock Enable
--	ddr3top_csn			: out std_logic;--								SSTL-15 CLASS I, Chip Select
--	ddr3top_odt			: out std_logic;--								SSTL-15 CLASS I, On-Die Termination Enable
--	ddr3top_wen			: out std_logic;--								SSTL-15 CLASS I, Write Enable
--	ddr3top_rstn		: out std_logic;--								1.5V, Reset
--	ddr3top_oct_rzq		: in std_logic;--								1.5V, OCT Pin for SSTL-15	
--
--	--Ethernet PHY interface
--	--IO: 20
--	enet_intn			: in std_logic;--								2.5V, MDIO Interrupt
--	enet_resetn			: out std_logic;--							2.5V, MDIO Interrupt
--	enet_mdio			: out std_logic;--							2.5V, MDIO Data
--	enet_mdc				: out std_logic;--							2.5V, MDIO Clock
--	--SGMII
--	enet_tx_p			: out std_logic;--							LVDS, SGMII Transmit Data
--	enet_rx_p			: in std_logic;--								LVDS, SGMII Receive Data
--	--RGMII
--	enet_tx_d			: out std_logic_vector(3 downto 0);--	2.5V, RGMII Transmit Data
--	enet_rx_d			: in std_logic_vector(3 downto 0);--	2.5V, RGMII Receive Data
--	enet_gtx_clk		: out std_logic;--							2.5V, RGMII Transmit Clock
--	enet_tx_en			: out std_logic;--							2.5V, RGMII Transmit Enable
--	enet_rx_clk			: in std_logic;--								2.5V, RGMII Receive Clock
--	enet_rx_dv			: in std_logic;--								2.5V, RGMII Receive Data Valid
--	
--	--Flash & SSRAM Interface
--	--IO: 86
--	fsm_a				: out std_logic_vector(26 downto 0);--		2.5V, Address
--	fsm_d				: inout std_logic_vector(31 downto 0);--	2.5V, Data	
--	flash_oen		: out std_logic;--								2.5V, Flash Output Enable
--	flash_wen		: out std_logic;--								2.5V, Flash Write Enable
--	flash_cen		: out std_logic_vector(1 downto 0);--		2.5V, Flash Chip Enable
--	flash_clk		: out std_logic;--								2.5V, Flash Clock 
--	flash_resetn	: out std_logic;--								2.5V, Flash Reset
--	flash_advn		: out std_logic;--								2.5V, Flash Address Valid 
--	flash_rdybsyn	: in std_logic_vector(1 downto 0);--		2.5V, Flash Ready/Busy
--	
--	sram_clk			: out std_logic;--							2.5V, SRAM Clock
--	sram_oen			: out std_logic;--							2.5V, SRAM Output Enable
--	sram_cen			: out std_logic;--							2.5V, SRAM Chip Enable
--	sram_bwen		: out std_logic;--							2.5V, SRAM Byte Write Enable
--	sram_bwn			: out std_logic_vector(3 downto 0);--	2.5V, SRAM Byte Write Per Byte
--	sram_adscn		: out std_logic;--							2.5V, SRAM Address Strobe Cntrl				
--	sram_gwn			: out std_logic;--							2.5V, SRAM Global Write Enable 
--	sram_zz			: out std_logic;--							2.5V, SRAM Sleep
--	sram_advn		: out std_logic;--							2.5V, SRAM Address Valid
--	sram_adspn		: out std_logic;--							2.5V, SRAM Address Strobe Proc
--	sram_mode		: out std_logic;--							2.5V, SRAM Burst Sequence Selection
--	sram_dqp			: out std_logic_vector(3 downto 0);--	2.5V, SRAM Parity Bits
--	
--	--MAX V Interface
--	--IO: 10
--	max5_clk			: out std_logic;--							2.5V, Max V Clk
--	max5_csn			: out std_logic;--							2.5V, Max V Chip Select
--	max5_ben			: out std_logic_vector(3 downto 0);--	2.5V, Max V Byte Enable Per Byte
--	max5_wen			: out std_logic;--							2.5V, Max V Write Enable
--	max5_oen			: out std_logic;--							2.5V, Max V Output Enable
--	int_tsd_sda		: inout std_logic;--							2.5V, Reserved for internal TSD I2C communication
--	int_tsd_scl		: in std_logic;--								2.5V, Reserved for Internal TSD I2C communication
--	
--	--USB Blaster II
--	--IO: 20
--	usb_clk				: in std_logic;--								2.5V, From Cypress USB PHY
--	usb_data				: out std_logic_vector(7 downto 0);--	2.5V, From MAXII
--	usb_addr				: out std_logic_vector(1 downto 0);--	2.5V, From MAXII
--	usb_scl				: inout std_logic;--							2.5V, From MAXII
--	usb_sda				: out std_logic;--							2.5V, From MAXII
--	usb_resetn			: out std_logic;--							2.5V, From MAXII
--	usb_oen				: out std_logic;--							2.5V, From MAXII
--	usb_rdn				: out std_logic;--							2.5V, From MAXII
--	usb_wrn				: out std_logic;--							2.5V, From MAXII
--	usb_full				: out std_logic;--							2.5V, From MAXII
--	usb_empty			: out std_logic;--							2.5V, From MAXII
--	fx2_resetn			: in std_logic;--								2.5V, From Cypress USB PHY
--	
--
--	--LCD character
--	--IO: 11
	lcd_data			: inout std_logic_vector(7 downto 0);--	2.5V, LCD Data
	lcd_csn			: out std_logic;--								2.5V, LCD Chip Select
	lcd_d_cn			: out std_logic;--								2.5V, LCD Data / Command Select
	lcd_wen			: out std_logic;--								2.5V, LCD Write Enable
--	
--	--user LED
--	--IO: 6
	user_led				: out std_logic_vector(3 downto 0);--	2.5V, Green User LEDs
--	hsma_tx_led			: out std_logic;--							2.5V, User LED - Labeled HSMA TX 
--	hsma_rx_led			: out std_logic;--							2.5V, User LED - Labeled HSMA RX
--	
--	--user DIP 
--	--IO: 4
	user_dipsw			: in std_logic_vector(3 downto 0);--	2.5V, User DIP Switches
--	
--	--user PB
--	--IO: 3
	user_pb				: in std_logic_vector(2 downto 0)--	2.5V, User Pushbuttons
--		
--	--Miscellaneous
--	--IO: 2
--	cpu_resetn			: in std_logic;--								2.5V, CPU Reset Pushbutton, comment out if dev_clrn function is set 
--	overtemp_fpga		: out std_logic --							2.5V, Reserved for internal TSD communication
	
	);
end entity;




----------------------------------------
------------ Architecture --------------
----------------------------------------

architecture rtl of arriav_bert is

constant N_channels : integer := 4;

------------------ Signal declaration ------------------------
signal user_dipsw_db						: std_logic_vector (3 downto 0);
signal user_pb_db							: std_logic_vector (2 downto 0);
signal user_rst_n							: std_logic;

signal tolcd								: std_logic_vector (127 downto 0);
signal tolcd_ena							: std_logic;
signal lcd_ready							: std_logic;
signal lcd_busy							: std_logic;

signal reconfig_busy						: std_logic;
signal tx_ready, rx_ready				: std_logic;
signal pll_locked, pll_locked1		: std_logic;
signal rx_syncstatus						: std_logic_vector (6 downto 0);
signal rx_patterndetect					: std_logic_vector (6 downto 0);
signal rx_is_lockedtodata				: std_logic_vector (6 downto 0);
signal rx_is_lockedtoref				: std_logic_vector (6 downto 0);
signal rx_signaldetect					: std_logic_vector (6 downto 0);
signal txrx_ready, txonly_ready		: std_logic;
signal txrx_rst_n							: std_logic;

signal tx_clk, rx_clk					: std_logic_vector (6 downto 0);
signal tx_data, rx_data					: std_logic_vector (55 downto 0);
signal tx_data_k							: std_logic_vector (6 downto 0);
signal rx_data_k							: std_logic_vector (6 downto 0);

signal counter								: std_logic_vector (7 downto 0);
signal counter_buf						: std_logic_vector (31 downto 0);
signal errcounter							: std_logic_vector (255 downto 0);
signal bitcounter							: std_logic_vector (63 downto 0);
signal errcounter_init					: std_logic_vector (7 downto 0);
signal errcounter_rst_n					: std_logic;
signal init_cnt							: std_logic_vector (2 downto 0);

signal refclk125							: std_logic;

type state_type is (SYNC, SENDING);
signal state 								: state_type;


begin

 debounce_i : debounce
 generic map (
	  N => 7,
	  C => X"00FF"
 )
 port map (
	  input(2 downto 0) => user_pb,
	  output(2 downto 0) => user_pb_db,
	  input(6 downto 3) => user_dipsw,
	  output(6 downto 3) => user_dipsw_db,
	  clk => clkin_50_top
 );
 
--
--clk_controller : altclkctrl
--port map(
--	inclk  							=> refclk2_ql1_p,
--	outclk 							=> refclk125
--);

refclk125 <= refclk2_qr1_p;

 
 -- LCD Display controller
lcd_c:lcd_controller
port map(
	din			=> tolcd,
	clock			=> clkin_50_top,
	reset_n		=> user_rst_n,
	enable_in	=> tolcd_ena,
	lcd_enable	=> lcd_csn,
	lcd_rs		=> lcd_d_cn,								
	lcd_rw		=> lcd_wen,					
	busy			=> lcd_busy,				
	lcd_data		=> lcd_data,
	ready_out	=> lcd_ready
);
 
txrx : txrx_wrapper 
port map (
	refclk125			=> refclk125,
	user_rst_n			=> txrx_rst_n,
	rx_serial_data		=> hsma_rx_p,
	tx_serial_data		=> hsma_tx_p,
	tx_clkout 			=> tx_clk,
	rx_clkout 			=> rx_clk,
	tx_data				=> tx_data,
	tx_data_k			=> tx_data_k,
	rx_data				=> rx_data,
	rx_data_k			=> rx_data_k,
	tx_ready				=> tx_ready,
	rx_ready				=> rx_ready,
	rx_is_lockedtoref => rx_is_lockedtoref,
	rx_is_lockedtodata => rx_is_lockedtodata,
	rx_signaldetect   => rx_signaldetect,  
	rx_syncstatus		=> rx_syncstatus,
	rx_patterndetect	=> rx_patterndetect,
	pll_locked			=> pll_locked,
	reconfig_busy		=> reconfig_busy
);

user_rst_n <= user_pb_db(0);
errcounter_rst_n <= user_pb_db(1);
user_led(0) <= txrx_ready;

-- ready logic for LEDs --
txrx_ready <= tx_ready and rx_ready and pll_locked and not reconfig_busy;

-- LCD input --
tolcd(127 downto 32) <= x"00000000" & rx_signaldetect (5 downto 2) & rx_is_lockedtoref (5 downto 2) & rx_is_lockedtodata (5 downto 2) & rx_patterndetect (5 downto 2) & tx_ready & rx_ready & pll_locked & reconfig_busy & tx_data_k (5 downto 2) & rx_data_k (5 downto 2) & rx_syncstatus (5 downto 2) & bitcounter (58 downto 27);
with user_dipsw_db (1 downto 0) select
   tolcd (31 downto 0) 	<= errcounter (95 downto 64) when "01",
									errcounter (159 downto 128) when "10",
									errcounter (223 downto 192) when "11",
									errcounter (31 downto 0) when others;

tolcd_ena <= '1';
user_led(3) <= lcd_ready and not lcd_busy;

-- transceiver init sequence
init: process(refclk125, user_rst_n)
begin
	if rising_edge(refclk125) then
		if (init_cnt < 4) then
			init_cnt <= init_cnt + '1';
         txrx_rst_n <= '1';
      else
         txrx_rst_n <= not user_rst_n;
      end if;
   end if;
end process;


-- counter as data generator
data_gen: process(refclk125, user_rst_n)
begin
	if(user_rst_n = '0') then
		counter <= (others => '0');
		state <= SYNC;
		bitcounter <= (others => '0');
	elsif (errcounter_rst_n = '0') then
		bitcounter <= (others => '0');
	elsif rising_edge(refclk125) then 
		
		case state is
			when SYNC =>
				tx_data_k <= "1111111";
				tx_data <= x"3C3C3C3C3C3C3C";		-- sync pattern
				user_led(2) <= '1';
				if (rx_syncstatus (5 downto 2) = x"F") then
					state <= SENDING;
				end if;
			
			when SENDING =>
				tx_data_k <= "0000000";
				tx_data <= counter & counter & counter & counter & counter & counter & counter;
				counter <= counter + '1';
				user_led(2) <= '0';
				bitcounter <= bitcounter + '1';
				if (rx_syncstatus (5 downto 2) /= x"F") then
					state <= SYNC;
				end if;
			when others =>
				state <= SYNC;
		end case;
				
	end if;
	end process;

-- check the data for consistency
G_1: for I in 0 to N_channels-1 generate
data_checker: process(rx_clk(I+2), user_rst_n, errcounter_rst_n)
	begin
		if(user_rst_n = '0' or errcounter_rst_n = '0') then
			errcounter(64*I + 63 downto 64*I) <= (others => '0');
			errcounter_init(2*I + 1 downto 2*I) <= (others => '0');
		elsif(rising_edge(rx_clk(5-I))) then
			if (errcounter_init(2*I + 1 downto 2*I) < 1) then
				errcounter_init(2*I + 1 downto 2*I) <= errcounter_init(2*I + 1 downto 2*I) + '1';
			elsif not(counter_buf (8*I + 7 downto 8*I) + '1' = rx_data (8*I + 23 downto 8*I+16)) then
				errcounter(64*I + 63 downto 64*I) <= errcounter(64*I + 63 downto 64*I) + '1';
			end if;
			counter_buf(8*I + 7 downto 8*I) <= rx_data (8*I + 23 downto 8*I+16);
		end if;
	end process;
end generate;
end rtl;
